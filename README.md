# Go Learning

Repository focused on explaining and exemplifying Go language features like data structures, flow controls, arithmetics, data types, etc.

## Prerequisites

What you need to start learning:

Go 1.7.5 (you can check the version running **go version** on your terminal)
```
Virtual Studio Code with Go extension (you can use whichever text editor you like, it's just a recommendation)
```

## Installing and running

You just need to clone the repository and run with Go:

git clone [https://gitlab.com/fel-siqueira/golangLearning.git](https://gitlab.com/fel-siqueira/golangLearning.git)
```
go run filename.go (or you can build the go files to binary executables, running **go build filename.go**, then running with ./filaname)
```

## Contributing

Feel free to contribute, every suggestion, improvement or correction is welcome. This repository is meant, mainly, to improve my knowledge about the Go language, and yours too!

## Authors

* **Felipe Siqueira Pinheiro**

## License

Use it as you want, preferentially, to learn or teach.