// It can be only one main package in the entire project
package main

import (
	"fmt"

	"gitlab.com/golangLearning/packages/structs"
)

var (
	lastname = "Siqueira"
)

// There can be only one func main() in the entire project: go run main.go
func main() {
	//hello.OtherHello()

	//variables.ListTypes()
	//variables.UsingVarStatement()
	//variables.UsingShortStatement()
	//variables.ZeroValues()
	//variables.MemAddress()
	//fmt.Println("\nOriginal variable value:", lastname)
	//variables.ChangeLastname(&lastname)
	//fmt.Println("Changed value using pointer:", lastname)
	//variables.ListEnvironmentVariables()

	//fmt.Println("AddTwoNumbers:", functions.AddTwoNumbers(1, 1))
	//fmt.Println("SubtractThreeNumbers:", functions.SubtractThreeNumbers(3, 2, 1))
	//variables.ShowActualUser()

	//firstReturn, secondReturn := functions.SwapArguments("Hello", "World")
	//fmt.Println("SwapArguments:", firstReturn, secondReturn)
	//functions.MultipleArguents(1, "Testing", "Viriadic", "Functions")
	//arrays.SliceDemo()

	var me structs.User
	fmt.Println(me)
	me.SetName()
	me.SetOpSys()
	fmt.Println(me)
	username := me.Name()
	opsys := me.OpSys()
	fmt.Println("My name is", username, "and I'm using", opsys)
}
