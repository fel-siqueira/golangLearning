All the project-specific libraries should be imported following the example below.
Every custom library/package should be named, and all it's methods/functions can be invoked like:
packagename.Function()