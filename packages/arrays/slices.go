package arrays

import (
	"fmt"
)

// SliceDemo will create a slice from an array, and then it will reslice the array
func SliceDemo() {
	array := [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	slice1 := array[0:5]
	slice1 = slice1[3:5]
	fmt.Println("Array:", array, "\nSlice1:", slice1)
}
