package variables

import "fmt"

// MemAddress will print the memory address for the declared variable
func MemAddress() {
	a := "Memory address"
	apointer := &a

	fmt.Println("The memory addres of A is:", apointer)
	fmt.Println("And the value of A is:", *apointer)
}

// ChangeLastname changes the variable value using pointers
func ChangeLastname(lastname *string) {
	*lastname = "Pinheiro"
}
