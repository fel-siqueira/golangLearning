// Check the variables.txt

package variables

import (
	"fmt"
)

// UsingVarStatement will declare, attribute a value and print a variable using the var statement
func UsingVarStatement() {
	var name string
	name = "Felipe"

	var lastname, age = "Siqueira", 20
	fmt.Println("Using the var statement:", name, lastname, age)
}

// UsingShortStatement will declare, attribute a value and print the variables using the := statement
func UsingShortStatement() {
	name := "Felipe"
	lastname, age := "Siqueira", 20

	fmt.Println("Using the short statement:", name, lastname, age)
}

// ZeroValues will declare three variables, but without any valued attribuition.
func ZeroValues() {
	var str string
	var integer int
	var boolean bool

	fmt.Println("Zero values:", str, integer, boolean)
}
