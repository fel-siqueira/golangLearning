package variables

import (
	"fmt"
	"reflect"
)

// ListTypes will print the datatype for each declared variable within the function
func ListTypes() {
	var (
		a string
		b int
		c float64
		d bool
		e *int
	)
	fmt.Println("A is a", reflect.TypeOf(a))
	fmt.Println("B is a", reflect.TypeOf(b))
	fmt.Println("C is a", reflect.TypeOf(c))
	fmt.Println("D is a", reflect.TypeOf(d))
	fmt.Println("E is a", reflect.TypeOf(e))
}
