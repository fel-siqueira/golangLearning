package variables

import (
	"fmt"
	"os"
)

// ListEnvironmentVariables will print an environment variable per line
func ListEnvironmentVariables() {
	for _, env := range os.Environ() {
		fmt.Println(env)
	}
}

// ShowActualUser will print the username
func ShowActualUser() {
	fmt.Println(os.Getenv("USER"))
}
