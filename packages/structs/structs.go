package structs

import (
	"os"
	"runtime"
)

type User struct {
	name string
	os   string
}

func (user *User) Name() string {
	return user.name
}

func (user *User) SetName() {
	user.name = os.Getenv("USER")
}

func (user *User) OpSys() string {
	return user.os
}

func (user *User) SetOpSys() {
	user.os = runtime.GOOS
}
