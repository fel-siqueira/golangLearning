package functions

// SwapArguments will take two strings as parameters and return them with swapped positions. If the input is Hello World, the output will be World Hello
func SwapArguments(x, y string) (string, string) {
	return y, x
}
