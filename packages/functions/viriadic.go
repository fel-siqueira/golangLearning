package functions

import "fmt"

// MultipleArguments will receive multiple arguments, viriadic function example
func MultipleArguments(number int, strings ...string) {
	fmt.Println(number)
	fmt.Println(strings)
}
