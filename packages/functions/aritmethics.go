package functions

// AddTwoNumbers will add the 2 parameters (x and y), both integers, and will return a integer.
func AddTwoNumbers(x int, y int) int {
	return x + y
}

// SubtractThreeNumbers will subtract the 2 parameters (x and y), both integers, and will return a integer.
func SubtractThreeNumbers(x int, y int, z int) int {
	return x - y - z
}
