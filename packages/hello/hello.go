// The line below represents the package name, that will be used to future imports.
// Every custom package should be commented, resuming what it does, and how to use it.

// Package hello contais a function that prints Hello World
package hello

import "fmt"

// Exportable functions MUST have your name started with uppercase, otherwise it won't be exported, and consequently not found by the main file.

// Hello function prints Hello World to the console, and breaks the line
func Hello() {
	fmt.Println("Hello World")
}
