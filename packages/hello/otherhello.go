package hello

import (
	"fmt"
	"runtime"
)

// OtherHello will print Hello from <operating system>
func OtherHello() {
	fmt.Println("Hello from", runtime.GOOS)
}
